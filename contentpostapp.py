import webapp

form = '<form action="" method="post" class="form-example" ' \
       + '<div class="form-example">' \
       + '<label for="recurso">Introduce el recurso: </label>' \
       + '<input type="text" name="recurso" id="recurso" required>' \
       + '</div>' \
       + '<div class="form-example">' \
       + '<input type="submit" value="Actualizar!">' \
       + '</div></form>'

class contentPostApp(webapp.webApp):
    recursos = {'/': "Esta es la p&aacute;gina principal", '/sandra': 'P&aacute;gina de Sandra'}

    def parse(self, received):
        metodo = received.decode().split(' ')[0]
        recurso = received.decode().split(' ')[1]
        if metodo == "POST":
            body = received.decode().split('\r\n\r\n')[1]
        else:
            body = None
        return metodo, recurso, body

    def process(self, arg):
        metodo, recurso, body = arg
        if (metodo == "POST"):
            self.recursos[recurso] = body
            http = "200 OK"
            html = "<html><body><h4>Introducir contenido</h4>Recurso solicitado: " + recurso + \
                   "<br>Contenido: " + self.recursos[recurso] + form + "</body></html>"
        elif recurso in self.recursos:
            http = "200 OK"
            html = "<html><body><h4>P&aacute;gina encontrada</h4>Recurso pedido: " + recurso + "<br>" \
                    + "Contenido: " + self.recursos[recurso] + "<p>Introducir nuevo:</p>" + form + "</body></html>"
        else:
            http = "404 ERROR NOT FOUND"
            html = "<html><body><h1>404 ERROR NOT FOUND" + form + "</h1></body></html>"
        return http, html


if __name__ == "__main__":
    contentpostapp = contentPostApp('localhost', 1234)